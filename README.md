# vagrant-IoT
WindowsでIoTするとき用のVagrantfileです。WSL2だと接続されたIoTデバイスを認識できないので、Virtualboxを使っています。

質問「[クロスコンパイル環境構築でglibcのmakeが失敗する](https://ja.stackoverflow.com/questions/35941/%E3%82%AF%E3%83%AD%E3%82%B9%E3%82%B3%E3%83%B3%E3%83%91%E3%82%A4%E3%83%AB%E7%92%B0%E5%A2%83%E6%A7%8B%E7%AF%89%E3%81%A7glibc%E3%81%AEmake%E3%81%8C%E5%A4%B1%E6%95%97%E3%81%99%E3%82%8B)」の開発環境が元です。[Dockerfile版](https://github.com/i0z0m/ubuntu-crossbuild)もあります。
